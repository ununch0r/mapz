﻿using GameLogic.GameEntities;
using GameLogic.SubjectEntities;
using System;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace UniversityLifeGameUI
{
	/// <summary>
	/// Interaction logic for LabPassingWindow.xaml
	/// </summary>
	public partial class LabPassingWindow : Window
	{
		public event Action<Subject> SubjectChosen;
		public LabPassingWindow()
		{
			InitializeComponent();
			InitializeComboBox();
		}
		private void InitializeComboBox()
		{
			var student = Student.GetInstance();
			foreach (var subject in student.Subjects)
			{
				LabComboBox.Items.Add(subject.Name);
			}
		}

		private void LabComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var student = Student.GetInstance();
			var subject = student.Subjects[LabComboBox.SelectedIndex];

			if (subject.Labs.FirstOrDefault(lab => lab.IsDone && !lab.IsPassed) != null)
			{
				LabNumberLabel.Content = subject.Labs.IndexOf(subject.Labs.FirstOrDefault(lab => lab.IsDone && !lab.IsPassed)) + 1;
				LabStatusLabel.Content = "Done.";
			}
			else
			{
				if(subject.Labs.All(lab => lab.IsDone))
				{
					LabStatusLabel.Content = "You've done all labs.";
				}
				else
				{
					LabStatusLabel.Content = "You don't have lab to pass.";
				}
			}
			ChangeProgressBar(subject);
		}

		private void ChangeProgressBar(Subject subject)
		{
			LabProgressBar.Minimum = 0;
			LabProgressBar.Maximum = subject.Labs.Count;
			var currentLab = subject.Labs.LastOrDefault(lab => lab.IsDone && lab.IsPassed);
			if (currentLab != null)
				LabProgressBar.Value = subject.Labs.IndexOf(currentLab) + 1;
		}

		private void PassLab_OnClick(object sender, RoutedEventArgs e)
		{
			var student = Student.GetInstance();
			var subject = student.Subjects[LabComboBox.SelectedIndex];
			OnSubjectChosen(subject);
			Close();
		}

		protected virtual void OnSubjectChosen(Subject subject)
		{
			if (subject.Labs.All(lab=>lab.IsDone && lab.IsPassed))
			{
				MessageBox.Show("You've passed all labs");
			}
			else if (subject.Labs.FirstOrDefault(lab => lab.IsDone && !lab.IsPassed) == null)
			{
				MessageBox.Show("This lab is not ready!");
			}
			else
			{
				SubjectChosen?.Invoke(subject);
			}
		}
	}
}
