﻿using System;
using System.Linq;
using GameLogic.GameEntities;
using GameLogic.StudyStrategy;
using GameLogic.SubjectEntities;
using System.Windows;
using System.Windows.Controls;

namespace UniversityLifeGameUI
{
	/// <summary>
	/// Interaction logic for LabChoosingWindow.xaml
	/// </summary>
	public partial class LabChoosingWindow : Window
	{

		public event Action<Subject> SubjectChosen;
		public LabChoosingWindow()
		{
			InitializeComponent();
			InitializeComboBox();
		}

		private void InitializeComboBox()
		{
			var student = Student.GetInstance();
			foreach (var subject in student.Subjects)
			{
				LabComboBox.Items.Add(subject.Name);
			}
		}

		private void LabComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var student = Student.GetInstance();
			var subject = student.Subjects[LabComboBox.SelectedIndex];
	

			SubjectNameLabel.Content = subject.Name;
			var currentLab = subject.Labs.FirstOrDefault(lab => !lab.IsDone);
			if (currentLab != null)
			{
				LabNumberLabel.Content = subject.Labs.IndexOf(subject.Labs.First(lab => !lab.IsDone)) + 1;
				LabStatusLabel.Content =  "Not ready.";
			}
			else
			{
				LabNumberLabel.Content = "You've done all labs.";
			}
			ChangeProgressBar(subject);
		}

		private void ChangeProgressBar(Subject subject)
		{
			LabProgressBar.Minimum = 0;
			LabProgressBar.Maximum = subject.Labs.Count;
			var currentLab = subject.Labs.LastOrDefault(lab => lab.IsDone && lab.IsPassed);
			if(currentLab != null)
				LabProgressBar.Value = subject.Labs.IndexOf(currentLab) + 1;
		}

		private void ChooseLab_OnClick(object sender, RoutedEventArgs e)
		{
			var student = Student.GetInstance();
			student.HowIStudy = new DoLabStrategy();

			var subject = student.Subjects[LabComboBox.SelectedIndex];
			OnSubjectChosen(subject);
			Close();
		}

		protected virtual void OnSubjectChosen(Subject subject)
		{
			if (subject.Labs.FirstOrDefault(lab => !lab.IsDone) != null)
			{
				SubjectChosen?.Invoke(subject);
			}
			else
			{
				MessageBox.Show("You've already done this lab");
			}
		}
	}
}
