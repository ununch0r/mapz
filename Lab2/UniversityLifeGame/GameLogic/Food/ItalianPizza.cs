﻿using GameLogic.Constants;

namespace GameLogic.Food
{
	public class ItalianPizza:Pizza
	{
		public ItalianPizza()
		{
			HungerDecrease = ValueConstants.ItalianPizzaHungerDecrease;
			Cost = ValueConstants.ItalianPizzaBalanceDecrease;
		}
		public override int HungerDecrease { get; set; }
		public override int Cost { get; set; }
	}
}
