﻿using GameLogic.Constants;

namespace GameLogic.Food
{
	public class UkrainianPizza:Pizza
	{
		public UkrainianPizza()
		{
			HungerDecrease = ValueConstants.UkrainianPizzaHungerDecrease;
			Cost = ValueConstants.UkrainianPizzaBalanceDecrease;
		}
		public override int HungerDecrease { get; set; }
		public override int Cost { get; set; }
	}
}

