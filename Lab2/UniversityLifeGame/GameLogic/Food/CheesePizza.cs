﻿using GameLogic.Constants;

namespace GameLogic.Food
{
	public class CheesePizza:PizzaDecorator
	{
		public CheesePizza(Pizza pizza) : base(pizza)
		{
			HungerDecrease = Pizza.HungerDecrease + ValueConstants.CheeseHungerDecrease;
			Cost = Pizza.Cost + ValueConstants.CheeseBalanceDecrease;
		}
		public override int HungerDecrease { get; set; }
		public override int Cost { get; set; }
	}
}
