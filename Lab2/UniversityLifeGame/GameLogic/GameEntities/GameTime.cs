﻿using System;
using GameLogic.Constants;
using GameLogic.Memento;


namespace GameLogic.GameEntities
{
	public sealed class GameTime
	{
		public event Action<DateTimeOffset> PropertyChanged;

		private static GameTime _instance;
		private static object _syncRoot = new object();
		public static readonly DateTimeOffset SessionDate
			= new DateTimeOffset(637130880000000000, new TimeSpan(0, 0, 0));

		private DateTimeOffset _currentGameTime;

		private GameTime()
		{
			CurrentGameTime = new DateTimeOffset(ValueConstants.StartTicks, new TimeSpan(0, 0, 0));
		}

		public static GameTime GetInstance()
		{
			if (_instance == null)
			{
				lock (_syncRoot)
				{
					if (_instance == null)
						_instance = new GameTime();
				}
			}
			return _instance;
		}

		public DateTimeOffset CurrentGameTime
		{
			get => _currentGameTime;
			set
			{
				_currentGameTime = value;
				PropertyChanged?.Invoke(_currentGameTime);
			}
		}

		public GameTimeMemento SaveState() => new GameTimeMemento(this);

		public void RestoreState(GameTimeMemento memento)
		{
			CurrentGameTime = memento.CurrentTime;
		}


		public void IncreaseTime(long ticks)
		{
			var currentTicks = CurrentGameTime.Ticks;
			currentTicks += ticks;
			CurrentGameTime = new DateTimeOffset(currentTicks, new TimeSpan(0, 0, 0));
		}
	}
}
