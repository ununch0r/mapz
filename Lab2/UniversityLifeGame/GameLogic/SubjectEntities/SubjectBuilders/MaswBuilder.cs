﻿using GameLogic.Helpers;

namespace GameLogic.SubjectEntities.SubjectBuilders
{
	public class MaswBuilder:SubjectBuilder
	{
		public override void SetCredits()
		{
			Subject.Credits = 7;
		}

		public override void SetLabPoints()
		{
			Subject.LabPoints = 40;
		}

		public override void SetLabs()
		{
			Subject.Labs = LabHelper.CreateMaswLabs();
		}
    }
}
