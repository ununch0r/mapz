﻿using GameLogic.Helpers;

namespace GameLogic.SubjectEntities.SubjectBuilders
{
	public class WebBuilder : SubjectBuilder
	{
		public override void SetCredits()
		{
			Subject.Credits = 5;
		}

		public override void SetLabPoints()
		{
			Subject.LabPoints = 40;
		}

		public override void SetLabs()
		{
			Subject.Labs = LabHelper.CreateWebLabs();
		}
	}
}
