﻿using GameLogic.Helpers;

namespace GameLogic.SubjectEntities.SubjectBuilders
{
	public class NmBuilder:SubjectBuilder
	{
		public override void SetCredits()
		{
			Subject.Credits = 5;
		}

		public override void SetLabPoints()
		{
			Subject.LabPoints = 30;
		}

		public override void SetLabs()
		{
			Subject.Labs = LabHelper.CreateNmLabs();
		}
    }
}
