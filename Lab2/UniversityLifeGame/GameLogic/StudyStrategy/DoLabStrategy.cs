﻿using System.Linq;
using GameLogic.SubjectEntities;

namespace GameLogic.StudyStrategy
{
	public  class DoLabStrategy:IStudyable
	{
		public void Study(Subject subject)
		{
			subject.Labs.First(lab=>!lab.IsDone).Do();
		}
	}
}
