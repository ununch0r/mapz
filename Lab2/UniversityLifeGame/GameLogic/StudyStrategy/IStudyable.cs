﻿using GameLogic.SubjectEntities;

namespace GameLogic.StudyStrategy
{
	public interface IStudyable
	{
		void Study(Subject subject);
	}
}
