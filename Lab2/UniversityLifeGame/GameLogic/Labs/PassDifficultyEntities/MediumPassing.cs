﻿using System;
using GameLogic.Constants;
using GameLogic.GameEntities;

namespace GameLogic.Labs.PassDifficultyEntities
{
	[Serializable]
    public class MediumPassing : PassDifficulty
    {
        public override void Pass()
        {
	        var student = Student.GetInstance();
	        var gameTime = GameTime.GetInstance();
            gameTime.IncreaseTime(ValueConstants.MediumPassing);
            student.DecreaseProductivity(ValueConstants.MediumPassing, ValueConstants.StudyingCoefficient);
        }
    }
}
