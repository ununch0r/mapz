﻿using GameLogic.Labs.PassDifficultyEntities;
using GameLogic.Labs.WorkTimeEntities;

namespace GameLogic.AbstractFactory
{
    public class LongHardLabFactory : LabFactory
    {
        public override PassDifficulty CreateDifficulty() => new HardPassing();
        public override WorkTime CreateWorkTime() => new LongWorkTime();
    }
}
