﻿using GameLogic.Labs.PassDifficultyEntities;
using GameLogic.Labs.WorkTimeEntities;

namespace GameLogic.AbstractFactory
{
    public class QuickEasyLabFactory:LabFactory
    {
        public override PassDifficulty CreateDifficulty() => new EasyPassing();
        public override WorkTime CreateWorkTime() => new QuickWorkTime();
    }
}
