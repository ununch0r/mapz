﻿using GameLogic.Constants;
using GameLogic.Food;
using GameLogic.GameEntities;
using GameLogic.Helpers;
using GameLogic.Locations;
using GameLogic.SubjectEntities;
using System;
using System.Linq;
using System.Threading.Tasks;
using GameLogic.Memento;

namespace GameLogic.Facade
{
	public class ActionFacade
	{
		private Student _student;
		private GameTime _gameTime;
		private SerializationHelper _serializationHelper;

		public ActionFacade()
		{
			_serializationHelper = new SerializationHelper();
			_gameTime = GameTime.GetInstance();
			_student = Student.GetInstance();
		}

		public void Eat()
		{
			_gameTime.IncreaseTime(ValueConstants.TimeToEat);
			var campus = _student.Location as Campus;
			var food = campus?.Fridge;
			if (food == null || food.Count < 1)
			{
				throw new Exception("Your fridge is empty!");
			}
			var pizza = food[^1];
			food.RemoveAt(food.Count - 1);
			_student.DecreaseHunger(pizza);
		}

		public void CallParents()
		{
			_student.Balance += 400;
			_gameTime.IncreaseTime(ValueConstants.TicksInOneHour);
			_student.IncreaseHunger(ValueConstants.TicksInOneHour);
		}

		public void ChangeLocation(Location location)
		{
			_student.Location = location;
			_gameTime.IncreaseTime(ValueConstants.TimeToChangeLocation);
			_student.DecreaseProductivity(ValueConstants.TimeToChangeLocation,
				ValueConstants.WalkingCoefficient);
			_student.IncreaseHunger(ValueConstants.TimeToChangeLocation);
		}

		public void Sleep()
		{
			_student.IncreaseProductivity(ValueConstants.TimeToSleep);
			_gameTime.IncreaseTime(ValueConstants.TimeToSleep);
			_student.IncreaseHunger(ValueConstants.TimeToSleep);
		}

		public void BuyPizza(Pizza pizza, Campus campus)
		{
			_student.Balance -= pizza.Cost;
			campus.Fridge.Add(pizza);
		}

		public void Study(Subject subject)
		{
			_student.Study(subject);
			_student.IncreaseHunger(ValueConstants.LongWorkTime);
			_student.DecreaseProductivity(ValueConstants.LongWorkTime, ValueConstants.StudyingCoefficient);
		}

		public void PassLab(Subject subject)
		{
			subject.Labs.FirstOrDefault(lab => lab.IsDone && !lab.IsPassed)?.Pass();
			subject.ExamProgress += 1 / subject.Labs.Count * subject.LabPoints;
		}

		public void SaveGame()
		{
			var studentState = _student.SaveState();
			_serializationHelper.SerializeObject(studentState, StringConstants.StudentXmlFilePath);

			var gameTimeState = _gameTime.SaveState();
			_serializationHelper.SerializeObject(gameTimeState, StringConstants.GameTimeXmlFilePath);
		}

		public void LoadGame()
		{
			var studentMemento =  _serializationHelper
				.DeSerializeObject<StudentMemento>(StringConstants.StudentXmlFilePath);
			_student.RestoreState(studentMemento);

			var timeMemento = _serializationHelper
				.DeSerializeObject<GameTimeMemento>(StringConstants.GameTimeXmlFilePath);
			_gameTime.RestoreState(timeMemento);
		}
	}
}
