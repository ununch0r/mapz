﻿namespace GameLogic.Constants
{
	public static class ExceptionMessages
	{
		public static readonly string StudentIsNotHungryMessage = "You are not hungry!";
	}
}
