﻿using GameLogic.AbstractFactory;
using GameLogic.Labs;
using System.Collections.Generic;

namespace GameLogic.Helpers
{
	public static class LabHelper
	{
		public static List<Lab> CreateMaswLabs()
		{
			var labs = new List<Lab>()
			{
				new Lab(new LongHardLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new LongMediumLabFactory())
			};
			return labs;
		}

		public static List<Lab> CreateAlgorithmsLabs()
		{
			var labs = new List<Lab>()
			{
				new Lab(new LongHardLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new LongMediumLabFactory()),
				new Lab(new LongMediumLabFactory()),
				new Lab(new LongMediumLabFactory()),
				new Lab(new LongHardLabFactory())
			};
			return labs;
		}

		public static List<Lab> CreateCaLabs()
		{
			var labs = new List<Lab>()
			{
				new Lab(new LongHardLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new LongMediumLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
			};
			return labs;
		}

		public static List<Lab> CreateElLabs()
		{
			var labs = new List<Lab>()
			{
				new Lab(new LongHardLabFactory()),
				new Lab(new LongMediumLabFactory()),
				new Lab(new LongMediumLabFactory()),
				new Lab(new LongMediumLabFactory())
			};
			return labs;
		}

		public static List<Lab> CreateNmLabs()
		{
			var labs = new List<Lab>()
			{
				new Lab(new LongHardLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new LongMediumLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory())
			};
			return labs;
		}

		public static List<Lab> CreateOcnLabs()
		{
			var labs = new List<Lab>()
			{
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new LongMediumLabFactory())
			};
			return labs;
		}

		public static List<Lab> CreateOopLabs()
		{
			var labs = new List<Lab>()
			{
				new Lab(new LongHardLabFactory()),
				new Lab(new LongHardLabFactory()),
				new Lab(new LongHardLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new LongHardLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new LongMediumLabFactory())
			};
			return labs;
		}

		public static List<Lab> CreateSdtLabs()
		{
			var labs = new List<Lab>()
			{
				new Lab(new LongHardLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new LongHardLabFactory()),
			};
			return labs;
		}

		public static List<Lab> CreateWebLabs()
		{
			var labs = new List<Lab>()
			{
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new LongMediumLabFactory()),
				new Lab(new LongMediumLabFactory())

			};
			return labs;
		}

		public static List<Lab> CreateOsLabs()
		{
			var labs = new List<Lab>()
			{
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickEasyLabFactory()),
				new Lab(new QuickMediumLabFactory()),
				new Lab(new QuickMediumLabFactory()),
			};
			return labs;
		}
	}
}
