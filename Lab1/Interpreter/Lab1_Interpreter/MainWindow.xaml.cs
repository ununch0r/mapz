﻿using System.Globalization;
using System.Windows;
using Lab1_Interpreter.Expressions.BaseExpressions;
using Lab1_Interpreter.Expressions.BinaryExpressions;
using Lab1_Interpreter.Expressions.UnaryExpressions;
using Lab1_Interpreter.MainUtilities;
using Lab1_Interpreter.Utilities;


namespace Lab1_Interpreter
{
    public partial class MainWindow : Window
    {
        Content _content;
        public MainWindow()
        {
            InitializeComponent();
        }
        private static string ReplaceWithWhiteBlanks(string input)
        {
            input = input.Replace("(", " ( ");
            input = input.Replace(")", " ) ");
            input = input.Replace("{", " { ");
            input = input.Replace("}", " } ");
            input = input.Replace("+", " + ");
            input = input.Replace("/", " / ");
            input = input.Replace("*", " * ");
            input = input.Replace("#", " # ");
            input = input.Replace("[", " [ ");
            input = input.Replace("]", " ] ");
            return input;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string input = ReplaceWithWhiteBlanks(Input.Text);

            trvMenu.Items.Clear();
            Output.Text = "";
            var lexer = new Lexer(input);

            lexer.Tokenize();
            _content = lexer.Content;

            var mainBlock = lexer.MainBlock;
            var root = new MenuItem() { Title = "BlockExpression" };

            foreach (var t in mainBlock.Trees)
            {
	            AddTreeItem(root, t);
            }

            trvMenu.Items.Add(root);

            foreach (var v in _content.Variables)
            {
                Output.Text += $"{v.Key} = {v.Value:0.00} \n";
            }
        }

        // I know... DRY, DRY, DRY, but I have no time =(
        private void AddTreeItem(MenuItem Root, IExpression expression)
        {
            if (expression is BlockExpression blocks)
            {
	            foreach (var block in blocks.Trees)
                {
                    var item = new MenuItem() { Title = block.GetType().Name };
                    Root.Items.Add(item);
                    AddTreeItem(item, block);
                }
            }
            else if (expression is BinaryExpression binary)
            {
	            if (!(binary is AssignmentExpression))
                    Root.Title += $" ( { binary.Interpret(_content):0.00} )";
                var item = new MenuItem() { Title = binary.FirstExpression.GetType().Name };
                Root.Items.Add(item);
                AddTreeItem(item, binary.FirstExpression);
                var item2 = new MenuItem() { Title = binary.SecondExpression.GetType().Name };
                Root.Items.Add(item2);
                AddTreeItem(item2, binary.SecondExpression);
            }
            else if (expression is IfExpression ifExpression)
            {
	            var ifItem = new MenuItem() { Title = ifExpression.GetType().Name };
                AddConditionItem(ifExpression, ifItem);
                if (ifExpression.NegativeBlockExpression != null)
                {
                    var item3 = new MenuItem() { Title = "(Negative case) " 
                                                         + ifExpression.NegativeBlockExpression.GetType().Name };
                    ifItem.Items.Add(item3);
                    AddTreeItem(item3, ifExpression.NegativeBlockExpression);
                }
                Root.Items.Add(ifItem);
            }

            else if (expression is WhileExpression whileExpr)
            {
	            var whileItem = new MenuItem() { Title = whileExpr.GetType().Name };
                AddConditionItem(whileExpr, whileItem);
                Root.Items.Add(whileItem);
            }


            else if (expression is UnaryExpression unary)
            {
	            var item = new MenuItem() { Title = unary.FirstExpression.GetType().Name };
                Root.Items.Add(item);
                AddTreeItem(item, unary.FirstExpression);
            }

            else if (expression is FormulaExpression)
            {
                var formula = expression as FormulaExpression;
                Root.Title += $" ( { formula.Interpret(_content):0.00} )";
                var item = new MenuItem() { Title = formula.expr1.GetType().Name };
                Root.Items.Add(item);
                AddTreeItem(item, formula.expr1);
            }

            else if (expression is FunctionExpression)
            {
                var function = expression as FunctionExpression;
                Root.Title += $" ( { function.Interpret(_content):0.00} )";
                var item = new MenuItem() { Title = function.expr1.GetType().Name.ToString() };
                Root.Items.Add(item);
                AddTreeItem(item, function.expr1);
            }

            else if (expression is NumberExpression number)
            {
	            var item = new MenuItem() { Title = number.Value.ToString(CultureInfo.InvariantCulture) };
                Root.Items.Add(item);
                return;
            }
            else if (expression is VariableExpression variable)
            {
	            var item = new MenuItem() { Title = variable.Name };
                Root.Items.Add(item);
                return;
            }
        }
        private void AddConditionItem(IExpression expression, MenuItem Root)
        {
            var conditionExpression = expression as ConditionExpression;
            if (conditionExpression != null)
            {
	            var item = new MenuItem() { Title = "(Condition) " + conditionExpression.Condition.GetType().Name };
	            Root.Items.Add(item);
	            AddTreeItem(item, conditionExpression.Condition);
            }

            if (conditionExpression == null) return;
            var item2 = new MenuItem() { Title = "(Positive case) " + conditionExpression.PositiveBlockExpression.GetType().Name };
            Root.Items.Add(item2);
            AddTreeItem(item2, conditionExpression.PositiveBlockExpression);
        }
    }
}
