﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Lab1_Interpreter.Expressions.BaseExpressions;
using Lab1_Interpreter.Expressions.BinaryExpressions;
using Lab1_Interpreter.Expressions.FormulaExpressions;
using Lab1_Interpreter.Expressions.UnaryExpressions;

namespace Lab1_Interpreter.MainUtilities
{
    public class Lexer
    {
        private readonly List<string> _variables;
        private readonly List<string> _functions;

        public readonly string input;
        private List<IExpression> _tokens;
        public List<IExpression> blocks;
        public BlockExpression MainBlock { get; private set; }
        public Content Content { get; private set; }
        public static int BlockCount { get; private set; } = 0;

        Parser _parser;
        IExpression _root;
        PostfixExpression _postfix;
        Stack<IExpression> _output;

        public Lexer(string input)
        {
            this.input = input;
            MainBlock = new BlockExpression();
            _tokens = new List<IExpression>();
            blocks = new List<IExpression>();
            _variables = new List<string>();
            _functions = new List<string>();
        }

        public void Tokenize()
        {
            var lines = input.Split('\n');

           Content = new Content();

 
            for (int i = 0, n = lines.Length; i < n; i++)
            {
                _tokens = AddTokens(lines[i]);
                _postfix = new PostfixExpression(Content);
                _postfix.CreatePostfixExpression(_tokens);
                _output = _postfix.output;

                if (_output.Count > 0 && _output.Peek() is IfExpression)
                {
                    AddIfExpression(lines, ref i, n);
                    MainBlock.Trees.Add(_root);
                }

                else if(_output.Count > 0 && _output.Peek() is WhileExpression)
                {
                    AddWhileExpression(lines, ref i, n);
                    MainBlock.Trees.Add(_root);
                }

                else 
                {
                    _parser = new Parser(_output);
                    _root = _parser.CreateTree();
                    switch (_root)
                    {
	                    case null:
		                    continue;
	                    case BlockExpression _:
		                    blocks.Clear();
		                    break;
                    }

                    if (BlockCount > 0)
	                    blocks.Add(_root);
                    else
	                    MainBlock.Trees.Add(_root);
                }
            }
            MainBlock.Interpret(Content);
        }


        private void AddIfExpression(string[] lines, ref int i, int n)
        {
            var ifExpression = _output.Pop() as IfExpression;
            _parser = new Parser(_output);
            _root = _parser.CreateTree();
            if (ifExpression == null) return;
            ifExpression.Condition = _root;
            if (i < n - 1 && lines[++i].Contains("{"))
            {
	            AddBlock(lines, ref i, n);
	            ifExpression.PositiveBlockExpression = _root;
            }

            if (i < n - 2 && lines[++i].Contains("else") && lines[++i].Contains("{"))
            {
	            AddBlock(lines, ref i, n);
	            ifExpression.NegativeBlockExpression = _root;
            }

            _root = ifExpression;
        }

        private void AddWhileExpression(string[] lines, ref int i , int n)
        {
          
             var wExpr = _output.Pop() as WhileExpression;
             _parser = new Parser(_output);
             _root = _parser.CreateTree();
             if (wExpr == null) return;
             wExpr.Condition = _root;
             if (i < n - 1 && lines[++i].Contains("{"))
             {
	             AddBlock(lines, ref i, n);
	             wExpr.PositiveBlockExpression = _root;
             }

             _root = wExpr;
        }

        private List<IExpression> AddTokens(string line)
        {
            _tokens = new List<IExpression>();
            var expressions = Regex.Split(line, @"([*+/\ )(])").ToList();
            expressions.RemoveAll(x => x == " " || x == "" || x == "\r");

            foreach (var token in expressions.Select(t => t.Trim('\r', ' '))
	            .Select(TokenizeExpressions)
	            .TakeWhile(token => !(token is CommentExpression)))
            {
	            _tokens.Add(token);
            }
            return _tokens;
        }


        private BlockExpression AddBlock(string[] lines, ref int i, int n)
        {
            blocks = new List<IExpression>();
            do
            {
                var addTokens = AddTokens(lines[++i]);
                lines[i] = lines[i].Trim('\r', ' ');
                _postfix = new PostfixExpression(Content);
                _postfix.CreatePostfixExpression(addTokens);
                _output = _postfix.output;

                if (_output.Count > 0 && _output.Peek() is IfExpression)
                {

                    AddIfExpression(lines, ref i, n);
                    i++;
                }
                else if (_output.Count > 0 && _output.Peek() is WhileExpression)
                {
                    AddWhileExpression(lines, ref i, n);
                    i++;
                }
                else if (lines[i] == "}")
                {
                    break;
                }
                else
                {
                    _parser = new Parser(_output);
                    _root = _parser.CreateTree();
                }
                blocks.Add(_root);
            } while (i < n - 1);

            var expressions = blocks.GetRange(0, blocks.Count);
            var block = new BlockExpression(expressions);
            _root = block;
            return null;
        }

    private IExpression TokenizeExpressions(string currentToken)
        {
            IExpression expression = null;
            
            switch(currentToken)
            {
                case "var": expression = new VarExpression();
                    break;
                case "=":   expression = new AssignmentExpression();
                    break;
                case "||":
                    expression = new OrExpression();
                    break;
                case "&&":
                    expression = new AndExpression();
                    break;
                case "==":
                    expression = new EqualityExpression();
                    break;
                case "!=":
                    expression = new NotEqualityExpression();
                    break;
                case ">":
                    expression = new MoreExpression();
                    break;
                case "<":
                    expression = new LessExpression();
                    break;
                case "+":
                    expression = new PlusExpression();
                    break;
                case "-":
                    expression = new MinusExpression();
                    break;
                case "*":
                    expression = new MultiplyExpression();
                    break;
                case "/":
                    expression = new DivisionExpression();
                    break;
                case "(":
                    expression = new LBracketExpression();
                    break;
                case ")":
                    expression = new RBracketExpression();
                    break;
                case "=>":
                    expression = new FunctionOperatorExpression();
                    break;
                case "if":
                    expression = new IfExpression();
                    break;
                case "while":
                    expression = new WhileExpression();
                    break;
                case"fn":
                    expression = new FnExpression();
                    break;
                case "{":
                    BlockCount++;
                    break;
                case "}":
                    BlockCount--;
                    List<IExpression> block = blocks.GetRange(0, blocks.Count);
                    expression = new BlockExpression(block);
                    break;
                case "sin":
                    expression = new SinExpression();
                    break;
                case "cos":
                    expression = new CosExpression();
                    break;
                case "tan":
                    expression = new TanExpression();
                    break;
                case "cot":
                    expression = new CotExpression();
                    break;
                case "abs":
                    expression = new AbsExpression();
                    break;
                case "pow":
                    expression = new PowExpression();
                    break;
                case "cbrt":
                    expression = new CbrtExpression();
                    break;
                case "sqrt":
                    expression = new SqrtExpression();
                    break;
                case "#":
                    expression = new CommentExpression();
                    break;
                default:
                    if ((new Regex("^-?[0-9]+$").IsMatch(currentToken))
                        || (new Regex(@"^-?[0-9][0-9,\.]+$").IsMatch(currentToken)))
                    {
                        var fmt = new NumberFormatInfo();
                        fmt.NegativeSign = "-";
                        expression = new NumberExpression(double.Parse(currentToken,fmt));
                    }
                    else if (currentToken.All(_ => char.IsLetterOrDigit(_)))
                    {
                        if (_functions.Any(x => x == currentToken))
                        {
                            expression = new FunctionExpression(currentToken);
                        }
                        else if (_variables.Any(x => x == currentToken))
                        {
                            expression = new VariableExpression(currentToken);
                        }
                        else
                        {
                            if (_tokens[_tokens.Count - 1] is FnExpression)
                            {
                                expression = new FunctionExpression(currentToken);
                                _functions.Add(currentToken);
                            }
                            else
                            {
                                expression = new VariableExpression(currentToken);
                                _variables.Add(currentToken);
                            }
                        }
                    }
                    break;
            }
            return expression;
        }

    }
}
