﻿using Lab1_Interpreter.Expressions.UnaryExpressions;
using System.Collections.Generic;
using Lab1_Interpreter.Expressions.BaseExpressions;
using Lab1_Interpreter.Expressions.BinaryExpressions;

namespace Lab1_Interpreter
{
    public class Parser
    {
        private Stack<IExpression> output;
        private IExpression root;
        public Parser(Stack<IExpression> output)
        {
            this.output = output;
        }

        public IExpression CreateTree()
        {
            if (output.Count > 0)
            {
                root = output.Pop();
                AddToTree(root);
                return root;
            }
            else
                return null;
        }

        private IExpression AddToTree(IExpression node)
        {
            try
            {
                switch (node)
                {
                    case BinaryExpression binaryExpression:
                        binaryExpression.SecondExpression = output.Pop();
                        AddToTree(binaryExpression.SecondExpression);
                        binaryExpression.FirstExpression = output.Pop();
                        AddToTree(binaryExpression.FirstExpression);
                        break;
                    case UnaryExpression ex1:
                        ex1.FirstExpression = output.Pop();
                        AddToTree(ex1.FirstExpression);
                        break;

                    default:
                        break;
                }
                return node;
            }
            catch
            {
                return null;
            }
        }


    }
}
