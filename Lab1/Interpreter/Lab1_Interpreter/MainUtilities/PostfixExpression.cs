﻿using System;
using System.Collections.Generic;
using Lab1_Interpreter.Expressions.BaseExpressions;
using Lab1_Interpreter.Expressions.BinaryExpressions;

namespace Lab1_Interpreter.MainUtilities
{
    public class PostfixExpression
    {
        private readonly Stack<IExpression> _stack;
        readonly Content _context;
        public Stack<IExpression> output { get; private set; }
        public PostfixExpression(Content context)
        {
            this._context = context;
            _stack = new Stack<IExpression>();
            output = new Stack<IExpression>();
        }
        public void CreatePostfixExpression(List<IExpression> expressions)
        {
            for (int i = 0; i < expressions.Count; i++)
            {

                if (expressions[i] is FormulaExpression formula)
                {
                    i++;
                    InsertFormulasIntoPostfix(expressions, ref i, formula);
                    output.Push(formula);
                    continue;
                }

                else if (expressions[i] is FnExpression)
                {
                    i++;
                    if (expressions[i] is FunctionExpression function)
                    {
                        i++;
                        InsertFunctionsIntoPostfix(expressions, ref i, function);
                    }
                    else
                    {
                        throw new Exception("Incorrect FirstExpression after FnExpression");
                    }

                    continue;
                }

                else if (expressions[i] is FunctionExpression functionCall)
                {
                    i++;
                    CreateFunctionContext(expressions, ref i, functionCall);
                    output.Push(functionCall);
                    continue;
                }

                else if (expressions[i] is NumberExpression || expressions[i] is VariableExpression)
                {
                    output.Push(expressions[i]);
                }

                else if (expressions[i] is BinaryExpression)
                {
                    var expression = expressions[i];
                    var peek = _stack.Peek();
                    while (_stack.Count > 0)
                    {
                        if (!(_stack.Peek() is BinaryExpression))
                            break;

                        if (expression.Priority <= peek.Priority)
                        {
                            output.Push(_stack.Pop());
                        }
                        else
                        {
                            break;
                        }
                    }
                    _stack.Push(expressions[i]);
                }

                else if (expressions[i] is LBracketExpression)
                {
                    _stack.Push(expressions[i]);
                }

                else if (expressions[i] is RBracketExpression)
                {
                    IExpression top = null;
                    while (_stack.Count > 0 && !((top = _stack.Pop()) is LBracketExpression))
                    {
                        output.Push(top);
                    }
                }
                else
                {
                    _stack.Push(expressions[i]);
                }
            }
            while (_stack.Count > 0)
            {
                var top = _stack.Pop();
                if (!expressions.Contains(top)) throw new ArgumentException();
                output.Push(top);
            }
        }

        private void InsertFormulasIntoPostfix(List<IExpression> expressions,
                                                            ref int currentPosition,
                                                            FormulaExpression formula)
        {
            var brackets = 0;
            var formulaBody = new List<IExpression>();
            for (int i = currentPosition; i < expressions.Count; i++)
            {
	            currentPosition = i;
	            switch (expressions[i])
	            {
		            case LBracketExpression _:
			            brackets++;
			            break;
		            case RBracketExpression _:
		            {
			            brackets--;
			            if (brackets < 0)
				            throw new Exception("Missing left bracket!");
			            var postfix = new PostfixExpression(_context);
			            postfix.CreatePostfixExpression(formulaBody);
			            var parser = new Parser(postfix.output);
			            formula.expr1 = parser.CreateTree();
			            return;
		            }
		            default:
			            formulaBody.Add(expressions[i]);
			            break;
	            }
            }
        }

        private void InsertFunctionsIntoPostfix(List<IExpression> expressions,
                                                            ref int currentPosition,
                                                            FunctionExpression function)
        {
            for (int i = currentPosition; i < expressions.Count; i++)
            {
	            currentPosition = i;

	            switch (expressions[i])
	            {
		            case VariableExpression variable:
			            function.localVariablesNames.Add(variable.Name);
			            break;
		            case FunctionOperatorExpression _:
			            currentPosition++;
			            CreateFunctionBodyPostfixExpression(expressions, ref currentPosition, function);
			            return;
	            }
            }
        }

        private void CreateFunctionBodyPostfixExpression(List<IExpression> expressions,
                                                            ref int currentPosition,
                                                            FunctionExpression function)
        {
            var brackets = 0;
            var functionBody = new List<IExpression>();
            for (int i = currentPosition; i < expressions.Count; i++)
            {
                currentPosition = i;
                brackets--;
                    if (brackets < 0)
                        throw new Exception("Missing left bracket!");
                    var postfix = new PostfixExpression(_context);
                    postfix.CreatePostfixExpression(functionBody);
                    var parser = new Parser(postfix.output);
                    function.expr1 = parser.CreateTree();
                    _context.SetFunction(function.name, function);
                    return;
            }
        }

        private void CreateFunctionContext(List<IExpression> expressions,
                                                            ref int currentPosition,
                                                            FunctionExpression function)
        {
            function.innerContext = _context.GetFunction(function.name).innerContext;
            function.expr1 = _context.GetFunction(function.name).expr1;
            function.localVariablesNames = _context.GetFunction(function.name).localVariablesNames;

            var brackets = 0;

            for (int i = currentPosition; i < expressions.Count; i++)
            {
	            currentPosition = i;
	            switch (expressions[i])
	            {
		            case LBracketExpression _:
			            brackets++;
			            break;
		            case RBracketExpression _:
		            {
			            brackets--;
			            if (brackets < 0)
				            throw new Exception("Missing left bracket!");
			            return;
		            }
		            case NumberExpression _:
		            case VariableExpression _:
			            function.localVariables.Add(expressions[i]);
			            break;
	            }
            }
        }

    }
}
