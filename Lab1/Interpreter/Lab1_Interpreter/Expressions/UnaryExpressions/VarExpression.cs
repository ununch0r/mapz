﻿using System;

namespace Lab1_Interpreter.Expressions.UnaryExpressions
{
    public class VarExpression : UnaryExpression
    {
        public override object Interpret(Content context)
        {
            var result = Convert.ToDouble(FirstExpression.Interpret(context));
            return result;
        }
    }
}
