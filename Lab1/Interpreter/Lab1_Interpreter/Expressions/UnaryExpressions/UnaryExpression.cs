﻿using Lab1_Interpreter.Expressions.BaseExpressions;

namespace Lab1_Interpreter.Expressions.UnaryExpressions
{
    public abstract class UnaryExpression : IExpression
    {
        public IExpression FirstExpression;

        public virtual Priority Priority => Priority.ZERO;
        public abstract object Interpret(Content context);
    }
}

