﻿namespace Lab1_Interpreter.Expressions.BaseExpressions
{
    public abstract class ConditionExpression : IExpression
    {
        public Priority Priority => Priority.ZERO;

        public IExpression Condition;
        public IExpression PositiveBlockExpression;


        protected ConditionExpression()
        {
            Condition = null;
            PositiveBlockExpression = null;
        }

        protected ConditionExpression(IExpression condition, IExpression positiveBlockExpression)
        {
            this.Condition = condition;
            this.PositiveBlockExpression = positiveBlockExpression;
        }
        public abstract object Interpret(Content context);
    }
}
