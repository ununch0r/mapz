﻿using System;

namespace Lab1_Interpreter.Expressions.BaseExpressions
{
    public class IfExpression : ConditionExpression
    {
        public IExpression NegativeBlockExpression;

        public IfExpression()
        {
            Condition = null;
            PositiveBlockExpression = null;
            NegativeBlockExpression = null;
        }
        public IfExpression(
	        IExpression condition,
	        IExpression positiveBlockExpression,
	        IExpression negativeBlockExpression
	        )
        {
            this.Condition = condition;
            this.PositiveBlockExpression = positiveBlockExpression;
            this.NegativeBlockExpression = negativeBlockExpression;
        }
        public override object Interpret(Content context)
        {
            bool cond = Convert.ToBoolean(Condition.Interpret(context));
            if(cond == true)
            {
                PositiveBlockExpression.Interpret(context);
            }
            else if(NegativeBlockExpression == null)
            {
                return null;
            }
            else
            {
                NegativeBlockExpression.Interpret(context);
            }
            return null;
        }
    }
}
