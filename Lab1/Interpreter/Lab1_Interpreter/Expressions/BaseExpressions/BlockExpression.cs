﻿using System.Collections.Generic;

namespace Lab1_Interpreter.Expressions.BaseExpressions
{
    public class BlockExpression : IExpression
    {
        public Priority Priority => Priority.ZERO;
        public List<IExpression> Trees;

        public BlockExpression()
        {
            Trees = new List<IExpression>();
        }
        public BlockExpression(List<IExpression> trees)
        {
            this.Trees = trees;
        }

        public object Interpret(Content context)
        {
            foreach(var tree in Trees)
            {
               tree.Interpret(context);
            }
            return null;
        }
    }
}
