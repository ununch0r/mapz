﻿namespace Lab1_Interpreter.Expressions.BaseExpressions
{
    public class VariableExpression : IExpression
    {
        public string Name { get; private set; }
        public double Value { get; private set; }
        public Priority Priority => Priority.TWO;

        public VariableExpression(string name)
        {
            Name = name;
        }

        public  object Interpret(Content context)
        {
            Value = context.GetVariable(Name);
            return Value;
        }
    }
}
