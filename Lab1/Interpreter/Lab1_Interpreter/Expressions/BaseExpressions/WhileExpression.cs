﻿using System;

namespace Lab1_Interpreter.Expressions.BaseExpressions
{
    public class WhileExpression : ConditionExpression
    {

        public WhileExpression()
        {
            Condition = null;
            PositiveBlockExpression = null;
        }
        public WhileExpression(IExpression condition, IExpression positiveBlockExpression)
        {
            this.Condition = condition;
            this.PositiveBlockExpression = positiveBlockExpression;
        }
        public override object Interpret(Content context)
        {
            bool cond = Convert.ToBoolean(Condition.Interpret(context));
            while (cond)
            {
                PositiveBlockExpression.Interpret(context);
                cond = Convert.ToBoolean(Condition.Interpret(context));
            }
            return null;
        }
    }
}
