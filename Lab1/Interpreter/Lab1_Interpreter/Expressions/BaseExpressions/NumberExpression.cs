﻿namespace Lab1_Interpreter.Expressions.BaseExpressions
{
    public class NumberExpression:IExpression
    {
        public double Value { get; private set; }
        public NumberExpression(double value)
        {
            this.Value = value;
        }

        public Priority Priority => Priority.TWO;

        public object Interpret(Content context)
        {
            return Value;
        }
    }
}
