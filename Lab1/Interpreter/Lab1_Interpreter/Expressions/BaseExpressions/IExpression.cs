﻿namespace Lab1_Interpreter.Expressions.BaseExpressions
{
    public interface  IExpression
    {
         Priority Priority { get;  }
         object Interpret(Content context);
    }

}
