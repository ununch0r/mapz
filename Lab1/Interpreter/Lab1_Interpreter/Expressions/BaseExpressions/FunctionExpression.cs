﻿using System.Collections.Generic;

namespace Lab1_Interpreter.Expressions.BaseExpressions
{
    public class FunctionExpression : IExpression
    {
        public string name { get; private set; }
        public IExpression expr1 { get; set; }

        public Content innerContext;
        public Priority Priority  => Priority.TWO; 

        public List<IExpression> localVariables { get; set; }
        public List<string> localVariablesNames { get; set; }

        public FunctionExpression(string value)
        {
            localVariables = new List<IExpression>();
            localVariablesNames = new List<string>();
            innerContext = new Content();
            this.name = value;
        }

        public object Interpret(Content context)
        {
            for( int i = 0; i< localVariables.Count;i++)
            {
                innerContext.SetVariable(localVariablesNames[i], (double)localVariables[i].Interpret(context));
            }

            return expr1.Interpret(innerContext);
        }
    }
}
