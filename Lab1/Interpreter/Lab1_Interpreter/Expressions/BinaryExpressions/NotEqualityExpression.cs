﻿using System;

namespace Lab1_Interpreter.Expressions.BinaryExpressions
{
    public class NotEqualityExpression : BinaryExpression
    {
        public override Priority Priority => Priority.TWO;

        public override object Interpret(Content context)
        {
            return Math.Abs(Convert.ToDouble(FirstExpression.Interpret(context)) - Convert.ToDouble(SecondExpression.Interpret(context)));

        }
    }
}
