﻿using System;

namespace Lab1_Interpreter.Expressions.BinaryExpressions
{
    public class MultiplyExpression : BinaryExpression
    {
         public override Priority Priority => Priority.FOUR;

         public override object Interpret(Content context)
        {
            return Convert.ToDouble(FirstExpression.Interpret(context)) * Convert.ToDouble(SecondExpression.Interpret(context));

        }
    }
}
