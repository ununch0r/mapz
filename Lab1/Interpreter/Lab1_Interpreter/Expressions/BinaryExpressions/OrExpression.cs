﻿using System;

namespace Lab1_Interpreter.Expressions.BinaryExpressions
{
    public class OrExpression : BinaryExpression
    {
        public override Priority Priority => Priority.ONE;

        public override object Interpret(Content context)
        {
            return Convert.ToBoolean(FirstExpression.Interpret(context)) || Convert.ToBoolean(SecondExpression.Interpret(context));

        }
    }
}
