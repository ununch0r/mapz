﻿using System;
using Lab1_Interpreter.Expressions.BaseExpressions;

namespace Lab1_Interpreter.Expressions.BinaryExpressions
{
	public class AssignmentExpression : BinaryExpression
	{

		public override object Interpret(Content context)
		{
			double value = 0;
			FirstExpression.Interpret(context);
			var variable = FirstExpression as VariableExpression;
			value = Convert.ToDouble(SecondExpression.Interpret(context));
			if (variable != null) context.SetVariable(variable.Name, value);
			return value;
		}
	}
}
