﻿using Lab1_Interpreter.Expressions.BaseExpressions;
using Lab1_Interpreter.Expressions.UnaryExpressions;

namespace Lab1_Interpreter.Expressions.BinaryExpressions
{
    public abstract class  BinaryExpression:UnaryExpression
    {
        public IExpression SecondExpression;
         public override Priority Priority => Priority.ONE;

         public abstract override object Interpret(Content context);
    }
}