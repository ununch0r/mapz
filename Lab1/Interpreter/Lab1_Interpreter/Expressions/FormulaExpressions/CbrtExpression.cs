﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    class CbrtExpression : FormulaExpression
    {
        public override object Interpret(Content context)
        {
            double res = Math.Cbrt(Convert.ToDouble(expr1.Interpret(context)));
            return res;
        }
    }
}
