﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    class AbsExpression : FormulaExpression
    {
        public override object Interpret(Content context)
        {
            double res = Math.Abs(Convert.ToDouble(expr1.Interpret(context)));
            return res;
        }
    }
}
