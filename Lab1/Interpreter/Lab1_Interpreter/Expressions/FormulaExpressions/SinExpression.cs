﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    public class SinExpression : FormulaExpression
    {
        public override object Interpret(Content context)
        {
            double res = Math.Sin(Convert.ToDouble(expr1.Interpret(context)));
            return res;
        }
    }
}
