﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    class PowExpression : FormulaExpression
    {
        public override object Interpret(Content context)
        {
            double res = Math.Pow(Convert.ToDouble(expr1.Interpret(context)),2);
            return res;
        }
    }
}
