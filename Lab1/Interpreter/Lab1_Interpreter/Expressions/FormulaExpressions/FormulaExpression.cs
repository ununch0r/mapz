﻿using System;
using System.Collections.Generic;
using System.Text;
using Lab1_Interpreter.Expressions.BaseExpressions;

namespace Lab1_Interpreter
{
    public abstract class FormulaExpression:IExpression
    {
        public IExpression expr1;
        public virtual Priority Priority { get { return Priority.TWO; } }
        public abstract object Interpret(Content context);
    }
}
