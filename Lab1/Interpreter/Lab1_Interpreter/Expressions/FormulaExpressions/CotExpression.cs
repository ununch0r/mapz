﻿using System;

namespace Lab1_Interpreter.Expressions.FormulaExpressions
{
    class CotExpression : FormulaExpression
    {
        public override object Interpret(Content context)
        {
            double res = (Math.Cos(Convert.ToDouble(expr1.Interpret(context)))/ Math.Sin(Convert.ToDouble(expr1.Interpret(context))));
            return res;
        }
    }
}
