﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    class CosExpression:FormulaExpression
    {
        public override object Interpret(Content context)
        {
            double res = Math.Cos(Convert.ToDouble(expr1.Interpret(context)));
            return res;
        }
    }
}
