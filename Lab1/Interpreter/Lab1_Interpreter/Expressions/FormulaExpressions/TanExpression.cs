﻿using System;

namespace Lab1_Interpreter.Expressions.FormulaExpressions
{
    class TanExpression : FormulaExpression
    {
        public override object Interpret(Content context)
        {
            double res = Math.Tan(Convert.ToDouble(expr1.Interpret(context)));
            return res;
        }
    }
}
