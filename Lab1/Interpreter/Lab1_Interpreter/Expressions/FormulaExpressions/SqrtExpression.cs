﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1_Interpreter
{
    class SqrtExpression : FormulaExpression
    {
        public override object Interpret(Content context)
        {
            double res = Math.Sqrt(Convert.ToDouble(expr1.Interpret(context)));
            return res;
        }
    }
}
