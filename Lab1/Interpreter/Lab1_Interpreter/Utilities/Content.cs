﻿using System.Collections.Generic;
using Lab1_Interpreter.Expressions.BaseExpressions;

namespace Lab1_Interpreter
{
    public class Content
    {
        public Dictionary<string, double> Variables { get; private set; }
        public Dictionary<string, FunctionExpression> Functions { get; private set; }

        public Content()
        {
            Variables = new Dictionary<string, double>();
            Functions = new Dictionary<string, FunctionExpression>();
        }

        public double GetVariable(string name)
        {
	        if (Variables.ContainsKey(name))
	        {
		        return Variables[name];
	        }
	        else
	        {
		        return 0;
	        }
        }

        public FunctionExpression GetFunction(string name)
        {
	        return Functions.ContainsKey(name) ? Functions[name] : null;
        }

        public void SetVariable(string name, double value)
        {
            if (Variables.ContainsKey(name))
            {
                Variables[name] = value;
            }
            else
            {
	            Variables.Add(name, value);
            }
        }

        public void SetFunction(string name, FunctionExpression value)
        {
            if (Functions.ContainsKey(name))
            {
                Functions[name] = value;
            }
            else
            {
	            Functions.Add(name, value);
            }
        }
    }
}
