﻿using System.Collections.ObjectModel;

namespace Lab1_Interpreter.Utilities
{

    public class MenuItem
    {
        public MenuItem()
        {
            Items = new ObservableCollection<MenuItem>();
        }

        public string Title { get; set; }

        public ObservableCollection<MenuItem> Items { get; set; }
    }
}
