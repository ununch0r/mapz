﻿namespace GameLogic.Phone
{
	public class BuyFoodbyPhoneCommand : ICommand
	{
		private readonly FoodShop _shop;

		public BuyFoodbyPhoneCommand(FoodShop shop)
		{
			_shop = shop;
		}
		public void Execute()
		{
			_shop.Buy();
		}

		public void Undo()
		{
			_shop.Return();
		}
	}
}
