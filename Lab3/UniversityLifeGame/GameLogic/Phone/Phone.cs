﻿namespace GameLogic.Phone
{
	public class Phone
	{
		private ICommand _command;

		public void SetCommand(ICommand command)
		{
			_command = command;
		}

		public void ExecuteButton()
		{
			_command.Execute();
		}

		public void CancelButton()
		{
			_command.Undo();
		}
	}
}
