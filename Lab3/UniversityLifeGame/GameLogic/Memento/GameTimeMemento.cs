﻿using System;
using System.Xml.Serialization;
using GameLogic.GameEntities;

namespace GameLogic.Memento
{
	[Serializable]
	public class GameTimeMemento
	{
		public GameTimeMemento()
		{
			
		}

		[XmlElement("CurrentTime")]
		public string CurrentTimeForXml 
		{
			get => CurrentTime.ToString("o");
			set => CurrentTime = DateTimeOffset.Parse(value);
		}

		[XmlIgnore]
		public DateTimeOffset CurrentTime { get; set; }



		public GameTimeMemento(GameTime gameTime)
		{
			CurrentTime = gameTime.CurrentGameTime;
		}
	}
}
