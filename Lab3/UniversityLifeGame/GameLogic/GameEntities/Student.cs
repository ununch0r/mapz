﻿using GameLogic.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using GameLogic.Annotations;
using GameLogic.Food;
using GameLogic.Locations;
using GameLogic.Memento;
using GameLogic.StudyStrategy;
using GameLogic.SubjectEntities;

namespace GameLogic.GameEntities
{
	public class Student : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;
		public event Action<Location> LocationChanged;

		private static readonly Lazy<Student> Lazy =
		new Lazy<Student>(() => new Student());

		public static Student GetInstance() => Lazy.Value;
		private Student()
		{
			Balance = ValueConstants.StartBalance;
			Productivity = ValueConstants.MaxProductivity;
			Hunger = ValueConstants.MaxHunger;
			Subjects = new List<Subject>();
			Name = "Victor";
			LastName = "Leonenko";
		}

		public IStudyable HowIStudy { private get; set; }
		private decimal _balance;
		private int _hunger;
		private int _productivity;
		private Location _location;

		public List<Subject> Subjects { get; set; }
		public string Name { get; set; }
		public string LastName { get; set; }
		public decimal Balance
		{
			get => _balance;
			set
			{
				_balance = value < 0 ? 0 : value;
				OnPropertyChanged(StringConstants.BalancePropertyPath);
			}
		}

		public int Productivity
		{
			get => _productivity;
			set
			{
				if (value > ValueConstants.MaxProductivity)
				{
					_productivity = ValueConstants.MaxProductivity;
				}
				else if (value < 0)
				{
					_productivity = 0;
				}
				else
				{
					_productivity = value;
				}
				OnPropertyChanged(StringConstants.ProductivityPropertyPath);
			}
		}

		public int Hunger
		{
			get => _hunger;
			set
			{
				if (value > ValueConstants.MaxHunger)
				{
					_hunger = ValueConstants.MaxHunger;
				}
				else if (value < 0)
				{
					_hunger = 0;
				}
				else
				{
					_hunger = value;
				}
				OnPropertyChanged(StringConstants.HungerPropertyPath);
			}
		}

		public Location Location
		{
			get => _location;
			set
			{
				_location = value;
				OnLocationChanged(_location);
			}
		}

		public void DecreaseHunger(Pizza pizza)
		{
			ValidateStudentIsHungry();

			Hunger += pizza.HungerDecrease;
		}

		public void IncreaseHunger(long actionTime)
		{
			var percentage = (double)actionTime / (double)ValueConstants.TicksInOneHour;
			var decreaseValue = percentage * 10;
			Hunger -= (int)decreaseValue;
		}

		public void IncreaseProductivity(long actionTime)
		{
			var percentage = (double)actionTime / (double)ValueConstants.TicksInOneHour;
			var increaseValue = percentage * 12.5;
			Productivity += (int)increaseValue;
		}

		public void DecreaseProductivity(long actionTime, double coefficient)
		{
			var percentage = (double)actionTime / (double)ValueConstants.TicksInOneHour;
			var decreaseValue = percentage * 10 * coefficient;
			Productivity -= (int)decreaseValue;
		}
		public void Study(Subject subject)
		{
			HowIStudy.Study(subject);
		}

		public StudentMemento SaveState() => new StudentMemento(this);

		public void RestoreState(StudentMemento memento)
		{
			Name = memento.Name;
			LastName = memento.LastName;
			Balance = memento.Balance;
			Hunger = memento.Hunger;
			Productivity = memento.Productivity;
			Subjects = memento.Subjects;
			Location = memento.Location;
		}


		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		private void ValidateStudentIsHungry()
		{
			if (Hunger == ValueConstants.MaxHunger)
			{
				throw new Exception(ExceptionMessages.StudentIsNotHungryMessage);
			}
		}

		protected virtual void OnLocationChanged(Location location)
		{
			LocationChanged?.Invoke(location);
		}
	}
}
