﻿using System;
using System.Xml.Serialization;

namespace GameLogic.Labs.PassDifficultyEntities
{
	[Serializable]
	[XmlInclude(typeof(EasyPassing)), XmlInclude(typeof(MediumPassing)), XmlInclude(typeof(HardPassing))]
	public abstract class PassDifficulty
    {
	    public PassDifficulty()
	    {
		    
	    }
        public abstract void Pass();
    }
}
