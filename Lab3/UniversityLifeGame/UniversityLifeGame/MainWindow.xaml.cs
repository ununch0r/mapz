﻿using GameLogic.Constants;
using GameLogic.Facade;
using GameLogic.Food;
using GameLogic.GameEntities;
using GameLogic.Locations;
using GameLogic.Phone;
using GameLogic.SubjectEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using UniversityLifeGameUI;
using Label = System.Windows.Controls.Label;

namespace UniversityLifeGame
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private readonly ActionFacade _actionsFacade = new ActionFacade();
		public List<Location> Locations;
		private Phone _phone = new Phone();
		private FoodShop _shop = new FoodShop();

		public MainWindow()
		{
			InitializeComponent();
			InitializeProgressBars();
			InitializeLocations();
			BindProperties();
		}


		private void EatFoodButton_OnClick(object sender, RoutedEventArgs e)
		{
			try
			{
				_actionsFacade.Eat();
			}
			catch (Exception exception)
			{
				MessageBox.Show(exception.Message);
			}

		}

		private void SleepButton_OnClick(object sender, RoutedEventArgs e)
		{
			try
			{
				_actionsFacade.Sleep();
			}
			catch (Exception exception)
			{
				MessageBox.Show(exception.Message);
			}

		}

		private void CallParentsButton_OnClick(object sender, RoutedEventArgs e)
		{
			try
			{
				_actionsFacade.CallParents();
			}
			catch (Exception exception)
			{
				MessageBox.Show(exception.Message);
			}

		}

		private void GoToCampusButton_OnClick(object sender, RoutedEventArgs e)
		{
			_actionsFacade.ChangeLocation(Locations.Single(location => location is Campus));
		}

		private void GoToNulpButton_OnClick(object sender, RoutedEventArgs e)
		{
			_actionsFacade.ChangeLocation(Locations.Single(location => location is University));
		}

		private void PizzaBought(Pizza pizza)
		{
			try
			{
				_shop.ChoosePizza(pizza);
				_phone.SetCommand(new BuyFoodbyPhoneCommand(_shop));
				_phone.ExecuteButton();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

		private void BuyPizzaWindow_Cancel()
		{
			try
			{
				_phone.CancelButton();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

		private void LabPassingWindow_SubjectChosen(Subject subject)
		{
			_actionsFacade.PassLab(subject);
		}

		private void LabChoosingWindow_SubjectChosen(Subject subject)
		{
			try
			{
				_actionsFacade.Study(subject);
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

		private void LectureListeningWindow_SubjectChosen(Subject subject)
		{
			try
			{
				_actionsFacade.Study(subject);
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}






		private void BuyFoodButton_OnClick(object sender, RoutedEventArgs e)
		{
			var buyPizzaWindow = new BuyPizzaWindow();
			buyPizzaWindow.PizzaBought += PizzaBought;
			buyPizzaWindow.Cancel += BuyPizzaWindow_Cancel;
			buyPizzaWindow.Show();
		}

		private void StudyButton_OnClick(object sender, RoutedEventArgs e)
		{
			var labChoosingWindow = new LabChoosingWindow();
			labChoosingWindow.SubjectChosen += LabChoosingWindow_SubjectChosen;
			labChoosingWindow.Show();
		}

		private void PassLabButton_OnClick(object sender, RoutedEventArgs e)
		{
			var labPassingWindow = new LabPassingWindow();
			labPassingWindow.SubjectChosen += LabPassingWindow_SubjectChosen;
			labPassingWindow.Show();
		}

		private void ListenLectureButton_OnClick(object sender, RoutedEventArgs e)
		{
			var lectureListeningWindow = new LectureListeningWindow();
			lectureListeningWindow.SubjectChosen += LectureListeningWindow_SubjectChosen;
			lectureListeningWindow.Show();
		}

		private void InitializeLocations()
		{
			Locations = new List<Location>
			{
				new Campus(),
				new University()
			};

			var student = Student.GetInstance();
			student.LocationChanged += Student_LocationChanged;
			student.Location = Locations.Find(location => location is Campus);
		}

		private void Student_LocationChanged(Location location)
		{
			ChangeBackground(location.BackgroundUrl);
			Visibility campusVisibility;
			Visibility nulpVisibility;
			if (location is Campus)
			{
				campusVisibility = Visibility.Visible;
				nulpVisibility = Visibility.Hidden;
			}
			else
			{
				campusVisibility = Visibility.Hidden;
				nulpVisibility = Visibility.Visible;
			}
			ChangeCampusButtons(campusVisibility);
			ChangeNulpButtons(nulpVisibility);
		}
		private void ChangeNulpButtons(Visibility visibility)
		{
			GoToCampusButton.Visibility = visibility;
			PassLabButton.Visibility = visibility;
			ListenLectureButton.Visibility = visibility;
		}

		private void ChangeCampusButtons(Visibility visibility)
		{
			EatFoodButton.Visibility = visibility;
			StudyButton.Visibility = visibility;
			SleepButton.Visibility = visibility;
			GoToNulpButton.Visibility = visibility;
		}
		private void BindProperties()
		{
			BindGameTime();
			BindStudentBalance();
			BindStudentHunger();
			BindStudentProductivity();
		}

		private void ChangeBackground(string uri)
		{
			var myBrush = new ImageBrush();
			var image = new Image
			{
				Source = new BitmapImage(new Uri(uri)
				)
			};
			myBrush.ImageSource = image.Source;
			MainGrid.Background = myBrush;
		}

		private void BindGameTime()
		{
			var gameTime = GameTime.GetInstance();
			gameTime.PropertyChanged += ChangeTime;
			gameTime.CurrentGameTime = new DateTimeOffset(
				ValueConstants.StartTicks, new TimeSpan(0, 0, 0));
			SessionLabel.Content = GameTime.SessionDate.UtcDateTime;
		}

		private void ChangeTime(DateTimeOffset currentGameTime)
		{
			TimeLabel.Content = currentGameTime.UtcDateTime;
		}

		private void BindStudentBalance()
		{
			var student = Student.GetInstance();
			var bind = new Binding
			{
				Source = student,
				Path = new PropertyPath(StringConstants.BalancePropertyPath),
				UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
			};
			BindingOperations.SetBinding(BalanceLabel, Label.ContentProperty, bind);
		}

		private void BindStudentProductivity()
		{
			var student = Student.GetInstance();
			var bind = new Binding
			{
				Source = student,
				Path = new PropertyPath(StringConstants.ProductivityPropertyPath),
				UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
			};
			BindingOperations.SetBinding(ProductivityProgressBar, ProgressBar.ValueProperty, bind);
		}

		private void BindStudentHunger()
		{
			var student = Student.GetInstance();
			var bind = new Binding
			{
				Source = student,
				Path = new PropertyPath(StringConstants.HungerPropertyPath),
				UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
			};
			BindingOperations.SetBinding(HungerProgressBar, ProgressBar.ValueProperty, bind);
		}

		private void InitializeProgressBars()
		{
			ProductivityProgressBar.Maximum = ValueConstants.MaxProductivity;
			HungerProgressBar.Maximum = ValueConstants.MaxHunger;
			ProductivityProgressBar.Minimum = 0;
			HungerProgressBar.Minimum = 0;
		}

		private void Save_OnClickAsync(object sender, RoutedEventArgs e)
		{
			_actionsFacade.SaveGame();
		}

		private void Load_OnClick(object sender, RoutedEventArgs e)
		{
			_actionsFacade.LoadGame();
		}
	}
}
