﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GameLogic.Facade;
using UniversityLifeGame;

namespace UniversityLifeGameUI
{
	/// <summary>
	/// Interaction logic for StartWindow.xaml
	/// </summary>
	public partial class StartWindow : Window
	{
		public StartWindow()
		{
			InitializeComponent();
		}

		private void LoadGameButton_OnClick(object sender, RoutedEventArgs e)
		{
			var facade = new ActionFacade();
			facade.LoadGame();
			var mainWindow = new MainWindow();
			mainWindow.Show();
			Close();
		}

		private void NewGameButton_OnClick(object sender, RoutedEventArgs e)
		{
			var startWindow = new SubjectChoosing();
			startWindow.Show();
			Close();
		}
	}
}
