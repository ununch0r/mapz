﻿using GameLogic.Constants;
using GameLogic.Lectures.ConcreteLectures;
using System.Collections.Generic;

namespace GameLogic.Lectures
{
    public class LecturesFactory
    {
        private Dictionary<string, Lecture> _lectures = new Dictionary<string, Lecture>();
        public LecturesFactory()
        {
            _lectures.Add(StringConstants.BoringLecturesKey, new BoringLecture());
            _lectures.Add(StringConstants.InterestingLecturesKey, new InterestingLecture());
        }

        public Lecture GetLecture(string key)
        {
	        return _lectures.ContainsKey(key) ? _lectures[key] : null;
        }
    }
}
