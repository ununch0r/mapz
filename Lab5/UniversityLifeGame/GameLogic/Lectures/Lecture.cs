﻿using System;
using GameLogic.SubjectEntities;

namespace GameLogic.Lectures
{
    [Serializable]
    public abstract class Lecture
    {
        public abstract void Listen(Subject subject);
    }
}
