﻿using GameLogic.SubjectEntities;

namespace GameLogic.Lectures.ConcreteLectures
{
    public class BoringLecture : Lecture
    {
        public override void Listen(Subject subject)
        {
            subject.ExamProgress += 7;
        }
    }
}
