﻿using GameLogic.SubjectEntities;

namespace GameLogic.Lectures.ConcreteLectures
{
    public class InterestingLecture : Lecture
    {
        public override void Listen(Subject subject)
        {
            subject.ExamProgress += 10;
        }
    }
}
