﻿using GameLogic.SubjectEntities;

namespace GameLogic.StudyStrategy
{
	public class ListenLectureStrategy:IStudyable
	{
		public void Study(Subject subject)
		{
			subject.Lecture.Listen(subject);
		}
	}
}
