﻿namespace GameLogic.Constants
{
	public static class StringConstants
	{
		public static readonly string BalancePropertyPath = "Balance";
		public static readonly string HungerPropertyPath = "Hunger";
		public static readonly string ProductivityPropertyPath = "Productivity";

		public static readonly string CampusBackground =
			"pack://application:,,,/UniversityLifeGameUI;component/Resources/campus_background.jpg";
		public static readonly string NulpBackground =
			"pack://application:,,,/UniversityLifeGameUI;component/Resources/nulp_background.png";

		public static readonly string BoringLecturesKey = "Boring";
		public static readonly string InterestingLecturesKey = "Interesting";
		public static readonly string StudentXmlFilePath = "StudentData.xml";
		public static readonly string GameTimeXmlFilePath = "GameTime.xml";

		public static readonly string SaveGameMessage = "Do you want to save game?";
		public static readonly string SaveGameWindowCaption = "Saving";
	}
}
