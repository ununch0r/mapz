﻿using System;
using GameLogic.Constants;
using GameLogic.GameEntities;

namespace GameLogic.Labs.PassDifficultyEntities
{
    [Serializable]
    public class EasyPassing : PassDifficulty
    {
        public override void Pass()
        {
	        var student = Student.GetInstance();
	        var gameTime = GameTime.GetInstance();
	        gameTime.IncreaseTime(ValueConstants.EasyPassing);
	        student.DecreaseProductivity(ValueConstants.EasyPassing, ValueConstants.StudyingCoefficient);
        }
    }
}
