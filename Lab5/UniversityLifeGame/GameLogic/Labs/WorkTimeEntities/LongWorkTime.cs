﻿using GameLogic.Constants;
using GameLogic.GameEntities;

namespace GameLogic.Labs.WorkTimeEntities
{
    public class LongWorkTime : WorkTime
    {
        public override void Do()
        {
	        var gameTime = GameTime.GetInstance();
            gameTime.IncreaseTime(ValueConstants.LongWorkTime);
        }
    }
}
