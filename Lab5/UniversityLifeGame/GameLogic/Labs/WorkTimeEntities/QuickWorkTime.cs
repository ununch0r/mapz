﻿using GameLogic.Constants;
using GameLogic.GameEntities;

namespace GameLogic.Labs.WorkTimeEntities
{
    public class QuickWorkTime : WorkTime
    {
        public override void Do()
        {
	        var gameTime = GameTime.GetInstance();
	        gameTime.IncreaseTime(ValueConstants.QuickWorkTime);
        }
    }
}
