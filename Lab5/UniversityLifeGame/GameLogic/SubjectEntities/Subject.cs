﻿using System;
using System.Collections.Generic;
using GameLogic.Constants;
using GameLogic.Labs;
using GameLogic.Lectures;

namespace GameLogic.SubjectEntities
{
	[Serializable]
	public class Subject
	{
		private static LecturesFactory _lecturesFactory = new LecturesFactory();

		public Subject()
		{

		}
		public Subject(string name)
		{
			Name = name;
			Labs = new List<Lab>();
			ExamProgress = 0;
		}
		public bool IsPassed { get; set; }
		public string Name { get; set; }
		public int LabPoints { get; set; }
		public int Credits { get; set; }
		public int ExamProgress { get; set; }
		public List<Lab> Labs { get; set; }
		public Lecture Lecture
		{
			get
			{
				var rand = new Random();
				string key;
				if (rand.Next(0, 10) % 2 == 0)
				{
					key = StringConstants.BoringLecturesKey;
				}
				else
				{
					key = StringConstants.InterestingLecturesKey;
				}
				var lecture = _lecturesFactory.GetLecture(key);
				return lecture;
			}
		}
	}
}
