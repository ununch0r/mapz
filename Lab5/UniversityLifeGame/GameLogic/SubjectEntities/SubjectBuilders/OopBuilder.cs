﻿using GameLogic.Helpers;

namespace GameLogic.SubjectEntities.SubjectBuilders
{
	public class OopBuilder : SubjectBuilder
	{
		public override void SetCredits()
		{
			Subject.Credits = 8;
		}

		public override void SetLabPoints()
		{
			Subject.LabPoints = 30;
		}

		public override void SetLabs()
		{
			Subject.Labs = LabHelper.CreateOopLabs();
		}
	}
}

