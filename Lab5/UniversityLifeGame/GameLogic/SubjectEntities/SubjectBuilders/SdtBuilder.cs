﻿using GameLogic.Helpers;

namespace GameLogic.SubjectEntities.SubjectBuilders
{
	public class SdtBuilder : SubjectBuilder
	{
		public override void SetCredits()
		{
			Subject.Credits = 3;
		}

		public override void SetLabPoints()
		{
			Subject.LabPoints = 60;
		}

		public override void SetLabs()
		{
			Subject.Labs = LabHelper.CreateSdtLabs();
		}
	}
}