﻿namespace GameLogic.SubjectEntities.SubjectBuilders
{
    public abstract class SubjectBuilder
    {
        public Subject Subject { get; protected set; }

        public void CreateSubject()
        {
	        Subject = new Subject(GetType().Name.Replace("Builder", ""));
        }
        public abstract void SetCredits();
        public abstract void SetLabPoints();
        public abstract void SetLabs();
    }
}
