﻿using System;
using System.Collections.Generic;
using GameLogic.GameEntities;
using GameLogic.Locations;
using GameLogic.SubjectEntities;

namespace GameLogic.Memento
{
	[Serializable]
	public class StudentMemento
	{
		public StudentMemento() { }
		public decimal Balance { get;  set; }
		public int Hunger { get;  set; }
		public int Productivity { get;  set; }
		public Location Location { get;  set; }
		public List<Subject> Subjects { get;  set; }
		public string Name { get; set; }
		public string LastName { get; set; }

		public StudentMemento(Student student)
		{
			Balance = student.Balance;
			Hunger = student.Hunger;
			Productivity = student.Productivity;
			Location = student.Location;
			Subjects = student.Subjects;
			Name = student.Name;
			LastName = student.LastName;
		}
	}
}
