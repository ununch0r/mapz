﻿using GameLogic.Constants;

namespace GameLogic.Food
{
	public class HawaiPizza:Pizza
	{
		public HawaiPizza()
		{
			HungerDecrease = ValueConstants.HawaiPizzaHungerDecrease;
			Cost = ValueConstants.HawaiPizzaBalanceDecrease;
		}
		public override int HungerDecrease { get; set; }
		public override int Cost { get; set; }
	}
}

