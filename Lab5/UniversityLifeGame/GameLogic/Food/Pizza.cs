﻿namespace GameLogic.Food
{
	public abstract class Pizza
	{
		public abstract int HungerDecrease { get; set; }
		public abstract int Cost { get; set; }

	}
}
