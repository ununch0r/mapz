﻿using GameLogic.Constants;

namespace GameLogic.Food
{
	public class TomatoPizza:PizzaDecorator
	{
		public TomatoPizza(Pizza pizza) : base(pizza)
		{
			HungerDecrease = Pizza.HungerDecrease + ValueConstants.TomatoHungerDecrease;
			Cost = Pizza.Cost + ValueConstants.TomatoBalanceDecrease;
		}

		public override int HungerDecrease { get; set; }
		public override int Cost { get; set; }
	}
}
