﻿using System;
using System.Runtime.Serialization;

namespace GameLogic.Exceptions
{
	[Serializable]
	public class NotPassedException : Exception
	{
		public NotPassedException()
		{
		}

		public NotPassedException(string message) : base(message)
		{
		}

		public NotPassedException(string message, Exception inner) : base(message, inner)
		{
		}

		protected NotPassedException(
			SerializationInfo info,
			StreamingContext context) : base(info, context)
		{
		}
	}
}
