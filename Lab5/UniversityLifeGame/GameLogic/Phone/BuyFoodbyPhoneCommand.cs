﻿namespace GameLogic.Phone
{
	public class BuyFoodByPhoneCommand : ICommand
	{
		private readonly FoodShop _shop;

		public BuyFoodByPhoneCommand(FoodShop shop)
		{
			_shop = shop;
		}
		public void Execute()
		{
			_shop.Buy();
		}

		public void Undo()
		{
			_shop.Return();
		}
	}
}
