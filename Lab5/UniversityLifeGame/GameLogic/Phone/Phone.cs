﻿using System;

namespace GameLogic.Phone
{
	public class Phone
	{
		private ICommand _command;

		public void SetCommand(ICommand command)
		{
			_command = command;
		}

		public void ExecuteButton()
		{
			_command.Execute();
		}

		public void CancelButton()
		{
			if (_command != null)
			{
				_command.Undo();
			}
			else
			{
				throw new Exception("You haven't bought anything!");
			}
		}
	}
}
