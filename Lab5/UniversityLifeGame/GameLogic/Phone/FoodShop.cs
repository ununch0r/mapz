﻿using System;
using GameLogic.Food;
using GameLogic.GameEntities;
using GameLogic.Locations;

namespace GameLogic.Phone
{
	public class FoodShop
	{
		private Pizza _pizza;

		public void ChoosePizza(Pizza pizza)
		{
			_pizza = pizza;
		}
		public void Buy()
		{
			var student = Student.GetInstance();
			var campus = student.Location as Campus;
			campus?.Fridge.Add(_pizza);
			student.Balance -= _pizza.Cost;
		}

		public void Return()
		{
			var student = Student.GetInstance();
			var campus = student.Location as Campus;

			if (campus?.Fridge.Remove(_pizza) == true)
			{
				student.Balance += _pizza.Cost;
			}
			else
			{
				throw new Exception("You have already return this pizza or it has been eaten");
			}
		}
	}
}
