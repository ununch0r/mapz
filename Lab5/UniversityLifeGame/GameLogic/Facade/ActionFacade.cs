﻿using GameLogic.Constants;
using GameLogic.Exceptions;
using GameLogic.GameEntities;
using GameLogic.Helpers;
using GameLogic.Locations;
using GameLogic.Memento;
using GameLogic.SubjectEntities;
using System;
using System.Linq;

namespace GameLogic.Facade
{
	public class ActionFacade
	{
		public event Action AllSubjectsPassed;
		private Student _student;
		private GameTime _gameTime;
		private SerializationHelper _serializationHelper;

		public ActionFacade()
		{
			_serializationHelper = new SerializationHelper();
			_gameTime = GameTime.GetInstance();
			_student = Student.GetInstance();
		}

		public void Eat()
		{
			_gameTime.IncreaseTime(ValueConstants.TimeToEat);
			var campus = _student.Location as Campus;
			var food = campus?.Fridge;
			if (food == null || food.Count < 1)
			{
				throw new Exception("Your fridge is empty!");
			}
			var pizza = food[^1];
			food.RemoveAt(food.Count - 1);
			_student.DecreaseHunger(pizza);
		}

		public void CallParents()
		{
			_student.Balance += 400;
			_gameTime.IncreaseTime(ValueConstants.TicksInOneHour);
			_student.IncreaseHunger(ValueConstants.TicksInOneHour);
		}

		public void ChangeLocation(Location location)
		{
			if (location is University && _student.Hunger < ValueConstants.MaxHunger / 2)
			{
				throw new InvalidOperationException("You can't go to the university when you are hungry!");
			}

			_student.Location = location;
			_gameTime.IncreaseTime(ValueConstants.TimeToChangeLocation);
			_student.DecreaseProductivity(ValueConstants.TimeToChangeLocation,
				ValueConstants.WalkingCoefficient);
			_student.IncreaseHunger(ValueConstants.TimeToChangeLocation);
		}

		public void Sleep()
		{
			if (_student.Hunger < ValueConstants.MaxHunger / 2)
			{
				throw new InvalidOperationException("You can't sleep when you are hungry!");
			}

			_student.IncreaseProductivity(ValueConstants.TimeToSleep);
			_gameTime.IncreaseTime(ValueConstants.TimeToSleep);
			_student.IncreaseHunger(ValueConstants.TicksInOneHour);
		}

		public void Study(Subject subject)
		{
			if (_student.Productivity < ValueConstants.MaxProductivity / 2)
			{
				throw new InvalidOperationException("You can't study when your productivity is low!");
			}

			_student.Study(subject);
			_student.IncreaseHunger(ValueConstants.LongWorkTime);
			_student.DecreaseProductivity(ValueConstants.LongWorkTime, ValueConstants.StudyingCoefficient);
		}

		public void PassLab(Subject subject)
		{
			if (_student.Hunger < ValueConstants.MaxHunger / 4)
			{
				throw new InvalidOperationException("You are hungry!");
			}

			subject.Labs.FirstOrDefault(lab => lab.IsDone && !lab.IsPassed)?.Pass();
				subject.ExamProgress += 1 / subject.Labs.Count * subject.LabPoints;
		}

		public void SaveGame()
		{
			var studentState = _student.SaveState();
			_serializationHelper.SerializeObject(studentState, StringConstants.StudentXmlFilePath);

			var gameTimeState = _gameTime.SaveState();
			_serializationHelper.SerializeObject(gameTimeState, StringConstants.GameTimeXmlFilePath);
		}

		public void LoadGame()
		{
			var studentMemento = _serializationHelper
				.DeSerializeObject<StudentMemento>(StringConstants.StudentXmlFilePath);
			_student.RestoreState(studentMemento);

			var timeMemento = _serializationHelper
				.DeSerializeObject<GameTimeMemento>(StringConstants.GameTimeXmlFilePath);
			_gameTime.RestoreState(timeMemento);
		}

		public void PassExam(Subject subject)
		{
			if (subject.ExamProgress > 51)
			{
				subject.IsPassed = true;
				if (_student.Subjects.All(subject => subject.IsPassed))
				{
					AllSubjectsPassed?.Invoke();
				}
			}
			else
			{
				throw new NotPassedException("You didn`t pass this exam.");
			}
		}

		protected virtual void OnAllSubjectsPassed()
		{
			AllSubjectsPassed?.Invoke();
		}
	}
}
