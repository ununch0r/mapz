﻿using GameLogic.Labs.PassDifficultyEntities;
using GameLogic.Labs.WorkTimeEntities;

namespace GameLogic.AbstractFactory
{
    public class QuickMediumLabFactory:LabFactory
    {
        public override PassDifficulty CreateDifficulty() => new MediumPassing();
        public override WorkTime CreateWorkTime() => new QuickWorkTime();
    }
}
