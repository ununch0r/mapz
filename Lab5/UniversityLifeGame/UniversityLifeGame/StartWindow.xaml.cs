﻿using GameLogic.Facade;
using System.Windows;
using UniversityLifeGame;

namespace UniversityLifeGameUI
{
	/// <summary>
	/// Interaction logic for StartWindow.xaml
	/// </summary>
	public partial class StartWindow : Window
	{
		public StartWindow()
		{
			InitializeComponent();
		}

		private void LoadGameButton_OnClick(object sender, RoutedEventArgs e)
		{
			var facade = new ActionFacade();
			facade.LoadGame();
			var mainWindow = new MainWindow();
			mainWindow.Show();
			Close();
		}

		private void NewGameButton_OnClick(object sender, RoutedEventArgs e)
		{
			var startWindow = new SubjectChoosing();
			startWindow.Show();
			Close();
		}
	}
}
