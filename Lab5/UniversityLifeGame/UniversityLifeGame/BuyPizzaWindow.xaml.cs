﻿using System;
using GameLogic.Food;
using System.Windows;

namespace UniversityLifeGameUI
{
	/// <summary>
	/// Interaction logic for BuyPizzaWindow.xaml
	/// </summary>
	public partial class BuyPizzaWindow : Window
	{
		public event Action<Pizza> PizzaBought;
		public event Action Cancel;
		public BuyPizzaWindow()
		{
			InitializeComponent();
		}

		private void BuyHawaiPizzaButton_OnClick(object sender, RoutedEventArgs e)
		{
			Pizza pizza = new HawaiPizza();
			if (HawaiPizzaDecorCheeseCheckBox.IsChecked??false)
			{
				pizza = new CheesePizza(pizza);
			}
			if (HawaiPizzaDecorTomatoCheckBox.IsChecked ?? false)
			{
				pizza = new TomatoPizza(pizza);
			}
			OnPizzaBought(pizza);
		}

		private void BuyUkrainianPizzaButton_OnClick(object sender, RoutedEventArgs e)
		{
			Pizza pizza = new UkrainianPizza();
			if (UkrainianPizzaDecorCheeseCheckBox.IsChecked ?? false)
			{
				pizza = new CheesePizza(pizza);
			}
			if (UkrainianPizzaDecorCheeseCheckBox.IsChecked ?? false)
			{
				pizza = new TomatoPizza(pizza);
			}
			OnPizzaBought(pizza);
		}

		private void BuyItalianPizzaButton_OnClick(object sender, RoutedEventArgs e)
		{
			Pizza pizza = new UkrainianPizza();
			if (ItalianPizzaDecorCheeseCheckBox.IsChecked ?? false)
			{
				pizza = new CheesePizza(pizza);
			}
			if (ItalianPizzaDecorTomatoCheckBox.IsChecked ?? false)
			{
				pizza = new TomatoPizza(pizza);
			}
			OnPizzaBought(pizza);
		}

		protected virtual void OnPizzaBought(Pizza pizza)
		{
			PizzaBought?.Invoke(pizza);
		}

		private void CancelButton_OnClick(object sender, RoutedEventArgs e)
		{
			OnCancel();
		}

		protected virtual void OnCancel()
		{
			Cancel?.Invoke();
		}
	}
}
