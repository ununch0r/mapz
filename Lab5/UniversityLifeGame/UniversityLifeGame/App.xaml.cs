﻿using System.Windows;
using UniversityLifeGameUI;

namespace UniversityLifeGame
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
	    private void ApplicationStart(object sender, StartupEventArgs e)
	    {
		    var startWindow = new StartWindow();
            startWindow.Show();
	    }
    }
}
