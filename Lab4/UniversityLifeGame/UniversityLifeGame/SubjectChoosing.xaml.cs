﻿using GameLogic.GameEntities;
using GameLogic.SubjectEntities.SubjectBuilders;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using UniversityLifeGame;

namespace UniversityLifeGameUI
{
	/// <summary>
	/// Interaction logic for SubjectChoosing.xaml
	/// </summary>
	public partial class SubjectChoosing : Window
	{
		public SubjectChoosing()
		{
			InitializeComponent();
			CreateSubjects();
			InitializeSubjectsGrid();
		}

		private void CreateSubjects()
		{
			var subjectConstructor = new SubjectConstructor();
			var builders = new List<SubjectBuilder>()
			{
				new CaBuilder(),
				new AlgorithmsBuilder(),
				new MaswBuilder(),
				new OsBuilder(),
				new NmBuilder(),
				new OcnBuilder(),
				new OopBuilder(),
				new WebBuilder(),
				new SdtBuilder(),
				new ElBuilder()
			};
			var student = Student.GetInstance();
			student.Subjects = builders.Select(builder => subjectConstructor.Construct(builder)).ToList();
		}

		private void InitializeSubjectsGrid()
		{
			var student = Student.GetInstance();
			var nameLabels = GetLabelsByName("SubjectName");
			var creditLabels = GetLabelsByName("SubjectCredits");
			var labsLabels = GetLabelsByName("SubjectLabs");
			for (var i = 0; i < student.Subjects.Count; i++)
			{
				nameLabels[i].Content = student.Subjects[i].Name;
				creditLabels[i].Content = student.Subjects[i].Credits;
				labsLabels[i].Content = student.Subjects[i].Labs.Count();
			}
		}



		private void OkButton_OnClick(object sender, RoutedEventArgs e)
		{
			var student = Student.GetInstance();
			var checkBoxes = GetCheckBoxes();
			var newSubjects = student.Subjects.Where((t, i) => checkBoxes[i].IsChecked ?? false).ToList();
			if (newSubjects.Count > 6 || newSubjects.Count < 4)
			{
				MessageBox.Show("You have to choose 4-6 subjects!");
			}
			else
			{
				student.Subjects = newSubjects;
				var mainWindow = new MainWindow();
				mainWindow.Show();
				this.Close();
			}
		}

		private List<Label> GetLabelsByName(string pattern)
		{
			var labels = new List<Label>();
			foreach (var element in MainGrid.Children)
			{
				var border = element as Border;
				if (!(border?.Child is Grid grid)) continue;
				foreach (UIElement gridChild in grid.Children)
				{
					if (!(gridChild is Label label)) continue;
					if (label.Name.Contains(pattern))
					{
						labels.Add(label);
					}
				}
			}
			return labels;
		}

		private List<CheckBox> GetCheckBoxes()
		{
			var checkBoxes = new List<CheckBox>();
			foreach (var element in MainGrid.Children)
			{
				var border = element as Border;
				if (!(border?.Child is Grid grid)) continue;
				foreach (UIElement gridChild in grid.Children)
				{
					if (gridChild is CheckBox checkBox)
					{
						checkBoxes.Add(checkBox);
					}
				}
			}
			return checkBoxes;
		}
	}
}
