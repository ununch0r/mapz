﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GameLogic.GameEntities;
using GameLogic.StudyStrategy;
using GameLogic.SubjectEntities;

namespace UniversityLifeGameUI
{
	/// <summary>
	/// Interaction logic for LectureListeningWindow.xaml
	/// </summary>
	public partial class LectureListeningWindow : Window
	{
		public event Action<Subject> SubjectChosen;
		public LectureListeningWindow()
		{
			InitializeComponent();
			InitializeComboBox();
		}

		private void LisctenLectureButton_OnClick(object sender, RoutedEventArgs e)
		{
			var student = Student.GetInstance();
			student.HowIStudy = new ListenLectureStrategy();

			var subject = student.Subjects[LectureComboBox.SelectedIndex];
			OnSubjectChosen(subject);
			Close();
		}

		private void LectureComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var student = Student.GetInstance();
			var subject = student.Subjects[LectureComboBox.SelectedIndex];
			ChangeProgressBar(subject);
		}

		private void ChangeProgressBar(Subject subject)
		{
			LabProgressBar.Minimum = 0;
			LabProgressBar.Maximum = 100;
			var progress = subject.ExamProgress;
			LabProgressBar.Value = progress;
		}

		private void InitializeComboBox()
		{
			var student = Student.GetInstance();
			foreach (var subject in student.Subjects)
			{
				LectureComboBox.Items.Add(subject.Name);
			}
		}

		protected virtual void OnSubjectChosen(Subject obj)
		{
			SubjectChosen?.Invoke(obj);
		}
	}
}
