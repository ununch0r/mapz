﻿using System;

namespace GameLogic.Constants
{
	public static class ValueConstants
	{
		public static readonly decimal StartBalance = 1300;
		public static readonly int MaxProductivity = 100;
		public static readonly int MaxHunger = 100;

		public static readonly long TicksInOneHour = TimeSpan.TicksPerHour;

		public static readonly long StartTicks = 637029792000000000;
		public static readonly long TimeToChangeLocation = TimeSpan.TicksPerHour / 2;
		public static readonly long TimeToEat = TimeSpan.TicksPerHour;
		public static readonly long TimeToSleep = TimeSpan.TicksPerHour*8;

		public static readonly long LongWorkTime = TimeSpan.TicksPerHour * 3;
		public static readonly long QuickWorkTime = TimeSpan.TicksPerHour;
		public static readonly long HardPassing = TimeSpan.TicksPerHour ;
		public static readonly long MediumPassing = TimeSpan.TicksPerHour / 2;
		public static readonly long EasyPassing = TimeSpan.TicksPerHour / 4;



		public static readonly double StudyingCoefficient = 2;
		public static readonly double WalkingCoefficient = 0.5;
		public static readonly int LecturesCount = 15;


		public static readonly int ItalianPizzaHungerDecrease = 80;
		public static readonly int ItalianPizzaBalanceDecrease = 70;
		public static readonly int HawaiPizzaHungerDecrease = 60;
		public static readonly int HawaiPizzaBalanceDecrease = 50;
		public static readonly int UkrainianPizzaHungerDecrease = 80;
		public static readonly int UkrainianPizzaBalanceDecrease = 70;
		public static readonly int CheeseHungerDecrease = 10;
		public static readonly int CheeseBalanceDecrease = 5;
		public static readonly int TomatoHungerDecrease = 10;
		public static readonly int TomatoBalanceDecrease = 5;

	}
}
