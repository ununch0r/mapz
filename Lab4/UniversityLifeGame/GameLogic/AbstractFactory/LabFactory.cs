﻿using GameLogic.Labs.PassDifficultyEntities;
using GameLogic.Labs.WorkTimeEntities;

namespace GameLogic.AbstractFactory
{
    public abstract class LabFactory
    {
        public abstract PassDifficulty CreateDifficulty();
        public abstract WorkTime CreateWorkTime();
    }
}
