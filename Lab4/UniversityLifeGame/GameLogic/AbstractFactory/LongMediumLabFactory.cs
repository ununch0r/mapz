﻿using GameLogic.Labs.PassDifficultyEntities;
using GameLogic.Labs.WorkTimeEntities;

namespace GameLogic.AbstractFactory
{
    public class LongMediumLabFactory:LabFactory
    {
        public override PassDifficulty CreateDifficulty() => new MediumPassing();
        public override WorkTime CreateWorkTime() => new LongWorkTime();
    }
}
