﻿using GameLogic.Helpers;

namespace GameLogic.SubjectEntities.SubjectBuilders
{
	public class CaBuilder:SubjectBuilder
	{
		public override void SetCredits()
		{
			Subject.Credits = 4;
		}

		public override void SetLabPoints()
		{
			Subject.LabPoints = 20;
		}

		public override void SetLabs()
		{
			Subject.Labs = LabHelper.CreateCaLabs();
		}
    }
}
