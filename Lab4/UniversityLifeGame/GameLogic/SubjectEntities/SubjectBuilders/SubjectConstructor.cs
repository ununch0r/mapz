﻿using System.Linq;

namespace GameLogic.SubjectEntities.SubjectBuilders
{
	public class SubjectConstructor
	{
		public Subject Construct(SubjectBuilder subjectBuilder)
		{
			subjectBuilder.CreateSubject();
			subjectBuilder.SetCredits();
			subjectBuilder.SetLabPoints();
			subjectBuilder.SetLabs();
			return subjectBuilder.Subject;
		}
	}
}
