﻿using GameLogic.Helpers;

namespace GameLogic.SubjectEntities.SubjectBuilders
{
	public class OsBuilder : SubjectBuilder
	{
		public override void SetCredits()
		{
			Subject.Credits = 6;
		}

		public override void SetLabPoints()
		{
			Subject.LabPoints = 40;
		}

		public override void SetLabs()
		{
			Subject.Labs = LabHelper.CreateOsLabs();
		}
	}
}
