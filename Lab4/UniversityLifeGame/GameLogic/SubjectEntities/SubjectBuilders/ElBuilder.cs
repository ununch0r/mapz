﻿using GameLogic.Helpers;

namespace GameLogic.SubjectEntities.SubjectBuilders
{
	public class ElBuilder:SubjectBuilder
	{
		public override void SetCredits()
		{
			Subject.Credits = 3;
		}

		public override void SetLabPoints()
		{
			Subject.LabPoints = 50;
		}

		public override void SetLabs()
		{
			Subject.Labs = LabHelper.CreateElLabs();
		}
	}
}
