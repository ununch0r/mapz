﻿using System.Linq;
using GameLogic.Helpers;

namespace GameLogic.SubjectEntities.SubjectBuilders
{
	public class AlgorithmsBuilder : SubjectBuilder
	{
		public override void SetCredits()
		{
			Subject.Credits = 7;
		}

		public override void SetLabPoints()
		{
			Subject.LabPoints = 45;
		}

		public override void SetLabs()
		{
			Subject.Labs = LabHelper.CreateAlgorithmsLabs();
		}
	}
}
