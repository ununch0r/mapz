﻿namespace GameLogic.Phone
{
	public interface ICommand
	{
		void Execute();
		void Undo();
	}
}
