﻿using System;
using System.Dynamic;
using GameLogic.AbstractFactory;
using GameLogic.Labs.PassDifficultyEntities;
using GameLogic.Labs.WorkTimeEntities;

namespace GameLogic.Labs
{
	[Serializable]
	public class Lab
	{
		public Lab()
		{
			
		}
		public bool IsDone { get; set; }
		public bool IsPassed { get; set; }
		public WorkTime WorkTime { get; set; }
		public PassDifficulty PassDifficulty { get; set; }

		public Lab(LabFactory factory)
		{
			WorkTime = factory.CreateWorkTime();
			PassDifficulty = factory.CreateDifficulty();
		}
		
		public void Do()
		{
			WorkTime.Do();
			IsDone = true;
		}

		public void Pass()
		{
			PassDifficulty.Pass();
			IsPassed = true;
		}
	}
}
