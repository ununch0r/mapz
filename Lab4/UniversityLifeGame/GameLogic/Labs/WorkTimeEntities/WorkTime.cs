﻿using System;
using System.Xml.Serialization;
using GameLogic.Locations;

namespace GameLogic.Labs.WorkTimeEntities
{
	[Serializable]
	[XmlInclude(typeof(LongWorkTime)), XmlInclude(typeof(QuickWorkTime))]
	public abstract class WorkTime
    {
	    public WorkTime()
	    {
		    
	    }
        public abstract void Do();
    }
}
