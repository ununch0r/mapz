﻿using System;
using GameLogic.Constants;
using GameLogic.GameEntities;

namespace GameLogic.Labs.PassDifficultyEntities
{
	[Serializable]
    public class HardPassing : PassDifficulty
    {
        public override void Pass()
        {
	        var student = Student.GetInstance();
	        var gameTime = GameTime.GetInstance();
	        gameTime.IncreaseTime(ValueConstants.HardPassing);
	        student.DecreaseProductivity(ValueConstants.HardPassing, ValueConstants.StudyingCoefficient);
        }
    }
}
