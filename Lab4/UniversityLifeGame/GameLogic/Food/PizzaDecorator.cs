﻿namespace GameLogic.Food
{
	public abstract class PizzaDecorator : Pizza
	{
		protected Pizza Pizza;

		protected PizzaDecorator(Pizza pizza)
		{
			Pizza = pizza;
		}
	}
}
