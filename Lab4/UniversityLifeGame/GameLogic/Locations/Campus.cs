﻿using System;
using System.Collections.Generic;
using GameLogic.Constants;
using GameLogic.Food;

namespace GameLogic.Locations
{
	[Serializable]
	public class Campus:Location
	{
		public Campus()
		{
			BackgroundUrl = StringConstants.CampusBackground;
			Fridge = new List<Pizza>();
		}

		public List<Pizza> Fridge { get; set; }
	}
}
