﻿using System;
using System.Xml.Serialization;

namespace GameLogic.Locations
{
	[Serializable]
	[XmlInclude(typeof(Campus)), XmlInclude(typeof(University))]
	public abstract class Location
	{
		public Location()
		{
			
		}
		public string BackgroundUrl { get; set; }
	}
}
